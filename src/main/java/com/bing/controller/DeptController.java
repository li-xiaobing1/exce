package com.bing.controller;

import com.bing.dao.DeptMaper;
import com.bing.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.List;

@Controller//增加该注解，表示当前类是控制类，springboot可以扫描
public class DeptController {
    @Autowired
    private DeptMaper deptMapper;//数据访问接口

    @GetMapping("/deptAll")
    public String FindDept(Model model){
        List<Dept> list = deptMapper.selectDept();
        System.out.println(list);
        model.addAttribute("list",list);
        return "department_list";
    }
    //跳转到增加页面
    @GetMapping("/toAddDept")
    public String toAddDept(){
        return "department_add";
    }
    //完成增加
    @PostMapping("/doAddDept")
    public String doAddDept(Dept dept){
        System.out.println(dept);
        int i=deptMapper.addDept(dept);
        System.out.println("执行结果"+i);
        return "redirect:/deptAll";//重定向，可以防止数据的重复提交
    }
    //根据ID查询，跳转到修改页面,需要传递要修改的部门实体
    @GetMapping("/toUpdateDept")
    public String toupdateDept(@RequestParam int id,Model model){
        Dept dept = deptMapper.selectDeptById(id);
        System.out.println(dept);
        model.addAttribute("dept",dept);
        return "department_update";
    }
    @PostMapping("doUpdateDept")
    public String doupdateDept(Dept dept){
        int i = deptMapper.updateDept(dept);
        return "redirect:/deptAll";
    }
    @GetMapping("/delDept")
   public String delDept(@RequestParam int id){
        int i = deptMapper.delDept(id);
        System.out.println("执行结果"+i);
        return "redirect:/deptAll";
    }
}
