package com.bing.dao;

import com.bing.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DeptMaper {
    List<Dept> selectDept(); //查询素有
    int addDept(Dept dept);//
    Dept selectDeptById(Integer id);
    int updateDept(Dept dept);
    int delDept(@Param("id") int id);

}
