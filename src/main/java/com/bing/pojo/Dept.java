package com.bing.pojo;

import java.util.Date;

public class Dept {
    private  Integer id;
    private  Integer department_number;
    private String name;
    private   String  manager;
    private String telephone;
    private  String address;
    private  String notes;

    public String getAddress() {
        return address;
    }

    public void setAddree(String addree) {
        this.address = addree;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDepartment_number() {
        return department_number;
    }

    public void setDepartment_number(Integer department_number) {
        this.department_number = department_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "id=" + id +
                ", department_number=" + department_number +
                ", name='" + name + '\'' +
                ", manager='" + manager + '\'' +
                ", telephone='" + telephone + '\'' +
                ", addree='" + address + '\'' +
                ", notes='" + notes + '\'' +
                '}'+"\n";
    }
}
